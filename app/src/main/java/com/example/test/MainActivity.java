package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.test.database.AppDatabase;
import com.example.test.entity.Message;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    AppDatabase database;

    private EditText etName;
    private EditText etEmail;
    private EditText etContent;
    private Button btnSend;
    private Spinner spinner;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = AppDatabase.getAppDatabase(this);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etContent = findViewById(R.id.etContent);
        btnSend = findViewById(R.id.btnSend);
        checkBox = findViewById(R.id.ck);

        String[] spinners = {"Gripe"};
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinners);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btnSend.setOnClickListener(this);
    }

    private void deleteAllMessage() {
        database.messageDAO().deleteAll();
    }

    private void deleteMessage(int id) {
        Message entity = database.messageDAO().getMessage(id);
        if(entity != null) {
            database.messageDAO().deleteMessage(entity);
        }
    }

    private void findMessage(int id) {
        Message entity = database.messageDAO().getMessage(id);
        Log.d("TAG", "Find message with id "+entity.getId());
    }

    private void getAllMessage() {
        List<Message> entities = database.messageDAO().getAllMessage();
        for(Message entity : entities) {
            Log.d("TAG", "id "+entity.getId() + ", message: " + entity.getContent());
        }
    }

    private void updateMessage(int id) {
        Message entity = database.messageDAO().getMessage(id);
        entity.setContent("This is content update");
        database.messageDAO().editMessage(entity);
    }

    private void insertMessage() {
        if(etName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter name please", Toast.LENGTH_SHORT).show();
            return;
        }
        if(etEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter email please", Toast.LENGTH_SHORT).show();
            return;
        }
        if(etContent.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter content please", Toast.LENGTH_SHORT).show();
            return;
        }
        Message msg = new Message();
        msg.setName(etName.getText().toString());
        msg.setEmail(etEmail.getText().toString());
        msg.setContent(etContent.getText().toString());
        msg.setDropdown(spinner.getSelectedItem().toString());
        if(!checkBox.isChecked()) {
            msg.setChecked(0);
        } else {
            msg.setChecked(1);
        }

        database.messageDAO().insertMessage(msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSend:
                insertMessage();
                break;
            default:
                break;
        }
    }
}