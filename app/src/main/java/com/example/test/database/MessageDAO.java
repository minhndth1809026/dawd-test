package com.example.test.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.test.entity.Message;

import java.util.List;

import io.reactivex.Completable;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MessageDAO {
    @Insert(onConflict = REPLACE)
    Completable insertMessage(Message msg);

    @Update
    void editMessage(Message msg);

    @Delete
    void deleteMessage(Message msg);

    @Query("select * from message")
    List<Message> getAllMessage();

    @Query("select * from message where id = :id")
    Message getMessage(int id);

    @Query("delete from message")
    void deleteAll();
}
